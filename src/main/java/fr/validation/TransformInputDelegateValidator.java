package fr.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author Thomas
 *
 */
public class TransformInputDelegateValidator<T, R> implements Validator<T> {

    private final Validator<R>   validator;

    private final Function<T, R> function;

    private final Predicate<R>   predicate;

    public TransformInputDelegateValidator(Validator<R> validator, Function<T, R> function) {
        this(validator, function, Objects::nonNull);
    }

    public TransformInputDelegateValidator(Validator<R> validator, Function<T, R> function, Predicate<R> predicate) {
        assert validator != null;
        assert function != null;
        assert predicate != null;

        this.validator = validator;
        this.function = function;
        this.predicate = predicate;
    }

    @Override
    public List<ConstraintViolation> validate(T input) {
        R transformedInput = this.function.apply(input);
        if (this.predicate.test(transformedInput)) {
            return this.validator.validate(transformedInput);
        }
        return new ArrayList<>();
    }

    /** @return the validator */
    public Validator<R> getValidator() {
        return this.validator;
    }

    /** @return the function */
    public Function<T, R> getFunction() {
        return this.function;
    }
}
