package fr.validation;



public interface Rule<T> {

    default boolean isApplicable(T input) {
        return true;
    };

    boolean apply(T input);

    ConstraintViolation constraintViolation(T input);
}
