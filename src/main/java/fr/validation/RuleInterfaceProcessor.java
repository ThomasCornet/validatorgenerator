package fr.validation;


import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

@SupportedAnnotationTypes({"fr.validation.GenerateRule"})
public class RuleInterfaceProcessor extends AbstractProcessor {

    private final Map<String, TypeElement> remainingRuleInterface = new HashMap<>();

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        boolean res = true;
        if (annotations.size() > 0) {
            populateRuleInterfaces(roundEnv);

            try {
                generateRuleInterfaces();
            } catch (IOException e) {
                e.printStackTrace();
                StringWriter writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                error(writer.toString(), null);
            }
            remainingRuleInterface.clear();
        }
        return res;
    }
    private void populateRuleInterfaces(RoundEnvironment roundEnv) {

        for (Element element : roundEnv.getElementsAnnotatedWith(GenerateRule.class)) {
            if (!isValidPojo(element)) {
                continue;
            } else {
                TypeElement typeElement = (TypeElement) element;
                remainingRuleInterface.put(typeElement.getQualifiedName().toString(), typeElement);

                List<? extends Element> allMembers = processingEnv.getElementUtils().getAllMembers(typeElement);

                List<ExecutableElement> methods = ElementFilter.methodsIn(allMembers);

                //TODO récursion

                for (ExecutableElement method : methods) {
                    if(method.getSimpleName().toString().startsWith("get")) {
                        Types types = this.processingEnv.getTypeUtils();
                        if(!method.getReturnType().getKind().isPrimitive()) {
                            TypeElement returnType = (TypeElement) types.asElement(method.getReturnType());
                            remainingRuleInterface.put(returnType.getQualifiedName().toString(), returnType);
                        }
                    }
                }

            }
        }
    }

    private boolean isValidPojo(Element element) {
        boolean res = true;
        if (element.getKind() == ElementKind.CLASS) {
            error(element, "@GenerateRuleInterface can only be used on class");
            res = false;
        }

        if (((TypeElement) element).getTypeParameters().size() > 0) {
            error(element, "Generic type parameters are not supported for simplicity");
            res = false;
        }
        return res;
    }

    private void error(Element element, String templateMessage, Object... args) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, String.format(templateMessage, args), element);
    }

    private void error(String msg, Element element) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, msg, element);
    }

}
