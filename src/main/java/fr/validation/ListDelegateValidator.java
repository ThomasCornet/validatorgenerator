package fr.validation;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Thomas
 *
 */
public class ListDelegateValidator<T> implements Validator<List<T>> {

    private final Validator<T> validator;

    public ListDelegateValidator(Validator<T> validator) {
        assert validator != null;

        this.validator = validator;
    }

    @Override
    public List<ConstraintViolation> validate(List<T> inputList) {
        List<ConstraintViolation> constraintViolations = new ArrayList<>();
        for (T input : inputList) {
            constraintViolations.addAll(this.validator.validate(input));
        }
        return constraintViolations;
    }

    /** @return the validator */
    public Validator<T> getValidator() {
        return this.validator;
    }
}
