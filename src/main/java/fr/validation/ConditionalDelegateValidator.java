package fr.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author Thomas
 *
 */
public class ConditionalDelegateValidator<T> implements Validator<T> {

    private final Validator<T> validator;

    private final Predicate<T> predicate;

    public ConditionalDelegateValidator(Validator<T> validator, Predicate<T> predicate) {
        assert validator != null;
        assert predicate != null;

        this.validator = validator;
        this.predicate = predicate;
    }

    @Override
    public List<ConstraintViolation> validate(T input) {
        if (this.predicate.test(input)) {
            return this.validator.validate(input);
        }
        return new ArrayList<>();
    }

    /** @return the validator */
    public Validator<T> getValidator() {
        return this.validator;
    }

    /** @return the predicate */
    public Predicate<T> getPredicate() {
        return this.predicate;
    }
}
