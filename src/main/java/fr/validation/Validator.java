package fr.validation;

import java.util.List;

public interface Validator<T> {

    List<ConstraintViolation> validate(T input);
}
