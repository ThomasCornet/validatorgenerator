package fr.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Thomas
 *
 */
public class BasicValidator<T, E extends Rule<T>> implements Validator<T> {

    protected final List<E>            rules;

    protected final List<Validator<T>> validators;

    public BasicValidator(List<E> rules, List<Validator<T>> validators) {
        assert rules != null;
        assert validators != null;

        this.rules = rules;
        this.validators = validators;
    }

    /** @see fr.validation.Validator#validate(java.lang.Object) */
    @Override
    public List<ConstraintViolation> validate(T input) {
        assert input != null;

        List<ConstraintViolation> constraintViolations = new ArrayList<>();
        for (Rule<T> rule : this.rules) {
            if (rule.isApplicable(input)) {
                if (!rule.apply(input)) {
                    constraintViolations.add(rule.constraintViolation(input));
                }
            }
        }
        for (Validator<T> validator : this.validators) {
            constraintViolations.addAll(validator.validate(input));
        }
        return constraintViolations;
    }

    /**
     * @return the rules
     */
    public List<E> getRules() {
        return this.rules;
    }

    /** @return the validators */
    public List<Validator<T>> getValidators() {
        return this.validators;
    }

    public static class Builder<T, E extends Rule<T>> {

        private final List<E>            rules      = new ArrayList<>();

        private final List<Validator<T>> validators = new ArrayList<>();

        /** Default constructor */
        public Builder() {
            //Default empty constructor
        }

        /**
         * Copy validator constructor
         *
         * @param basicValidator
         */
        public Builder(BasicValidator<T, E> basicValidator) {
            this.rules.addAll(basicValidator.getRules());
            this.validators.addAll(basicValidator.getValidators());
        }

        /**
         * Copy constructor
         *
         * @param builder
         */
        public Builder(Builder<T, E> builder) {
            this.rules.addAll(builder.rules);
            this.validators.addAll(builder.validators);
        }

        public Builder<T, E> addRule(E rule) {
            this.rules.add(rule);
            return this;
        }

        public Builder<T, E> addAllRule(Collection<E> rules) {
            this.rules.addAll(rules);
            return this;
        }

        public Builder<T, E> addValidator(Validator<T> validator) {
            this.validators.add(validator);
            return this;
        }

        public Builder<T, E> addAllValidator(Collection<Validator<T>> validators) {
            this.validators.addAll(validators);
            return this;
        }

        public Builder<T, E> but() {
            return new Builder<>(this);
        }

        public List<E> buildRules() {
            return Collections.unmodifiableList(new ArrayList<>(this.rules));
        }

        public List<Validator<T>> buildValidators() {
            return Collections.unmodifiableList(new ArrayList<>(this.validators));
        }

        public BasicValidator<T, E> build() {
            return new BasicValidator<>(this.buildRules(), this.buildValidators());
        }
    }
}
